public class Test {
    public static void main(String[] args) {
        Student model  = getStudentFromDatabase();

        StudentView view = new StudentView();

        StudentController controller = new StudentController(model, view);

        controller.updateView();

        controller.setStudentName("Paul","Whesley");
        controller.setStudentID(100);

        controller.updateView();
    }
    private static Student getStudentFromDatabase()
    {
        Student student = new Student("Ian","Somerheld",101);
        return student;
    }
}
