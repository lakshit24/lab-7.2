public class StudentController {
   Student model;
   StudentView view;

    public StudentController(Student model, StudentView view)
    {
        this.model = model;
        this.view = view;
    }

    public void setStudentName(String fname, String lname)
    {
        model.setFirstName(fname);
        model.setLastName(lname);
    }

    public String getStudentName()
    {
        return model.getFirstName()+" "+model.getLastName();
    }

    public void setStudentID(int id)
    {
        model.setStudentId(id);
    }

    public int getStudentId()
    {
        return model.getStudentId();
    }

    public void updateView()
    {
        view.printStudentDetails(model);
    }
}